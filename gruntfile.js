/**
 * Created by tekin on 21/01/2017.
 */

module.exports = function (grunt) {

    var iPathHtml = 'app/view/',
        iPathCss = 'app/scss/',
        iPathJs = 'app/js/',
        iPathImg = 'app/img/',
        ePathHtml = 'web/',
        ePathCss = 'web/css/',
        ePathJs = 'web/js/',
        ePathImg = 'web/img/';

    grunt.initConfig({

        sprite: {
            all: {
                src: iPathImg + 'icons/*.png',
                dest: ePathImg + 'sprite.png',
                imgPath: '../img/sprite.png',
                destCss: iPathCss + '_icon-sprites.scss'
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none',
                    noCache: true
                },
                files: [{
                    expand: true,
                    cwd: iPathCss,
                    src: ['*.scss'],
                    dest: ePathCss,
                    ext: '.css'
                }]
            }
        },

        browserify: {
            dist: {
                files: {
                    [iPathJs + 'output.expanded.js']: [iPathJs + 'index.js']
                }
            }
        },

        uglify: {
            my_target: {
                files: {
                    [ePathJs + 'output.js']: [iPathJs + 'output.expanded.js']
                }
            }
        },

        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                    expand: true,
                    cwd: iPathHtml,
                    src: ['*.html', '!_*.html'],
                    dest: ePathHtml
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', ['sprite', 'htmlmin', 'sass', 'browserify', 'uglify']);
    grunt.registerTask('watch', ['default']);

};
