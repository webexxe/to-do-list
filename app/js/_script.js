/**
 * Created by tekin on 21/01/2017.
 */

module.exports = function () {

    // ADD TASK
    // -------------------------------------------------------------------------/
    $(".form-add").on('click', function () {
        
        var target = $(".todo-list"),
            title = $("input[name='title']"),
            desc = $("textarea[name='desc']"),
            form = {};
        form.title = title.val();
        form.desc = desc.val();
        form.id = new Date().valueOf();

        //ITEM CONTAINER
        var desc_ = form.desc ? '<p></p>' : '',
            item = "<div class='item new' id='item-" + form.id + "'><input type='checkbox' id='item-c-" + form.id + "'><label for='item-c-" + form.id + "' class='item-action'><i class='icon-ncheck'></i><i class='icon-check'></i><span class='title'></span></label><div class='item-delete'><i class='icon-tool'></i></div>" + desc_ + "</div>";

        if (form.title) {
            target.append(item);

            //HTML ESCAPE
            $('#item-' + form.id + ' .title').text(form.title);
            if (form.desc) $('#item-' + form.id + ' p').text(form.desc);

            //FORM CLEAR
            title.val('');
            desc.val('');

            //NEW EFFECT
            setTimeout(function () {
                $('#item-' + form.id).removeClass("new");
            }, 100);

        } else {
            alert("Bir değer girmelisinizi...");
        }
    });

    // REMOVE TASK
    // -------------------------------------------------------------------------/
    $(document).delegate('.item-delete', 'click', function () {
        $(this).parent().remove();
    });

};